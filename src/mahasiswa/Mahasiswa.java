/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package mahasiswa;

/**
 *
 * @author pusim
 */
import java.sql.*;

public class Mahasiswa {

    private static final String URL = "jdbc:mysql://localhost:3306/mahasiswa";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public static void main(String[] args) {
        try {
            // Langkah 1: Membuat koneksi
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            // Langkah 2: Membuat statement
            Statement statement = connection.createStatement();

            // Langkah 3: Menjalankan query
            String queryCreateTable = "CREATE TABLE IF NOT EXISTS tbl_mahasiswa (nim INT PRIMARY KEY, nama VARCHAR(255), alamat VARCHAR(200), telp VARCHAR(25))";
            statement.executeUpdate(queryCreateTable);

            // Langkah 4: Menambahkan data
            insertData(statement, 12345, "Beni", "Gambiran", "08576574001");

            // Langkah 5: Membaca data
            readData(statement);

            // Langkah 6: Mengupdate data
            //updateData(statement, 1, "Jane Doe", "654321");

            // Langkah 7: Membaca data setelah diupdate
            readData(statement);

            // Langkah 8: Menghapus data
            deleteData(statement, 1);

            // Langkah 9: Membaca data setelah dihapus
            readData(statement);

            // Langkah 10: Menutup statement dan koneksi
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void insertData(Statement statement, int nim, String nama, String alamat, String telp) throws SQLException {
        String queryInsert = "INSERT INTO tbl_mahasiswa (nim, nama, alamat, telp) VALUES (" + nim + ", '" + nama + "', '" + alamat + "', '" + telp + "')";
        statement.executeUpdate(queryInsert);
        System.out.println("Data berhasil ditambahkan.");
    }

    private static void readData(Statement statement) throws SQLException {
        String querySelect = "SELECT * FROM tbl_mahasiswa";
        ResultSet resultSet = statement.executeQuery(querySelect);

        System.out.println("Data Mahasiswa:");
        while (resultSet.next()) {
            int nim = resultSet.getInt("nim");
            String nama = resultSet.getString("nama");
            String alamat = resultSet.getString("alamat");
            String telp = resultSet.getString("telp");
            System.out.println("NIM: " + nim + ", Nama: " + nama + ", ALAMAT: " + alamat + ", TELP: " + telp);
        }
        System.out.println();
    }

    private static void updateData(Statement statement, int nim, String nama, String alamat, String telp) throws SQLException {
        String queryUpdate = "UPDATE tbl_mahasiswa SET nama='" + nama + "', alamat='" + alamat + "', telp='" + telp + "' WHERE nim=" + nim;
        statement.executeUpdate(queryUpdate);
        System.out.println("Data berhasil diupdate.");
    }

    private static void deleteData(Statement statement, int nim) throws SQLException {
        String queryDelete = "DELETE FROM tbl_mahasiswa WHERE nim=" + nim;
        statement.executeUpdate(queryDelete);
        System.out.println("Data berhasil dihapus.");
    }
}
